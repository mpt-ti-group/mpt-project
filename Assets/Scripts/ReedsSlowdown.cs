﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReedsSlowdown : MonoBehaviour
{
    private float oldMovementForce = 0f;
    private float oldMovementTorque = 0f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Reeds collision");
            SoundManager.PlaySound("reeds_hit");
            PlayerController playerCtrl = collision.gameObject.GetComponent<PlayerController>();
            oldMovementForce = playerCtrl.movementForce;
            playerCtrl.movementForce *= 0.5f;
            oldMovementTorque = playerCtrl.movementTorque;
            playerCtrl.movementTorque *= 2f;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Reeds decollision");
            PlayerController playerCtrl = collision.gameObject.GetComponent<PlayerController>();
            playerCtrl.movementForce = oldMovementForce;
            playerCtrl.movementTorque = oldMovementTorque;
        }
    }
}
