﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateWater : MonoBehaviour
{
    [SerializeField] private float speedX = 0.1f;
    [SerializeField] private float speedY = 0.1f;
    [SerializeField] private float amplitudeX = 0.1f;
    [SerializeField] private float amplitudeY = 0.1f;
    [SerializeField] private float freqX = 0.1f;
    [SerializeField] private float freqY = 0.1f;

    private float curX;
    private float curY;

    private void Start()
    {
        curX = GetComponent<Renderer>().material.mainTextureOffset.x;
        curY = GetComponent<Renderer>().material.mainTextureOffset.y;

        // GetComponent<Renderer>().material.mainTextureScale = new Vector2(10.0f, 10.0f);

        freqX *= 2 * Mathf.PI;
        freqY *= 2 * Mathf.PI;
    }

    private void Update()
    {
        curX += Time.deltaTime * (speedX + amplitudeX * Mathf.Sin(freqX * Time.time));
        curY += Time.deltaTime * (speedY + amplitudeY * Mathf.Sin(freqY * Time.time));
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(curX, curY));
        // GetComponent<Renderer>().material.mainTextureOffset = new Vector2(curX, curY);

        //print(GetComponent<Renderer>().material.mainTextureOffset);
    }
}