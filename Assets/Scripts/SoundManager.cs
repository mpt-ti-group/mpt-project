﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip playerHit, paddle_1, paddle_2, reeds_hit;
    static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        playerHit = Resources.Load<AudioClip>("hit");
        paddle_1 = Resources.Load<AudioClip>("paddle_1");
        paddle_2 = Resources.Load<AudioClip>("paddle_2");
        reeds_hit = Resources.Load<AudioClip>("reeds_hit");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string sound)
    {
        switch (sound)
        {
            case "hit":
                audioSource.PlayOneShot(playerHit);
                break;
            case "paddle_1":
                audioSource.PlayOneShot(paddle_1);
                break;
            case "paddle_2":
                audioSource.PlayOneShot(paddle_2);
                break;
            case "reeds_hit":
                audioSource.PlayOneShot(reeds_hit);
                break;
        }
    }
}
