﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CountdownTimer : MonoBehaviour
{
    private float currTime = 0f;
    private bool stopTimer = false;
    private int topTimesTableSize = 10;
    private List<float> times = new List<float>();
    private string levelName;

    [SerializeField] private Text coundtownTimer = null;
    [SerializeField] private Text completionTime = null;
    [SerializeField] private Text leaderBoard = null;
    [SerializeField] private GameObject restartButton = null;
    [SerializeField] private GameObject mainMenuButton = null;

    private void Start()
    {
        // PlayerPrefs.DeleteAll(); // use this to clear all saved scores
        levelName = SceneManager.GetActiveScene().name;
        loadTopTimes();
        completionTime.text = "";
        leaderBoard.text = "";
        currTime = -Time.fixedDeltaTime;
    }

    // zaktualizuj czas
    private void Update()
    {
        if (stopTimer)
        {
            leaderBoard.text = getTableContent();
            restartButton.SetActive(true);
            mainMenuButton.SetActive(true);
        }
        else
        {
            currTime += Time.deltaTime;
            coundtownTimer.text = currTime.ToString("0.00");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !stopTimer)
        {
            times.Add(currTime);
            string message = "Your time: ";
            if (currTime == times.Min())
            {
                message = "New best time! ";
            }
            completionTime.text = message + currTime.ToString("0.00");

            saveTopTimes();
            stopTimer = true;
        }
    }

    private string getTableContent()
    {
        string table = "";
        table += "Position".PadRight(20);
        table += "Time".PadRight(20);
        table += "\n_________________\n\n";

        int howMany = times.Count < topTimesTableSize ? times.Count : topTimesTableSize;
        for (var i = 0; i < howMany; i++)
        {
            table += (i + 1).ToString("00").PadRight(23);
            table += times[i].ToString("0.00").PadRight(20);
            table += "\n";
        }

        return table;
    }

    private void loadTopTimes()
    {
        for (var i = 0; i < topTimesTableSize; i++)
        {
            float time = PlayerPrefs.GetFloat(levelName + "_topTimes_" + i);
            if (time > 0)
            {
                times.Add(time);
            }
        }
    }

    private void saveTopTimes()
    {
        times.Sort();
        int howMany = times.Count < topTimesTableSize ? times.Count : topTimesTableSize;
        for (var i = 0; i < howMany; i++)
        {
            PlayerPrefs.SetFloat(levelName + "_topTimes_" + i, times[i]);
        }
    }
}