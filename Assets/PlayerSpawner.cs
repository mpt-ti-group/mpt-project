﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private Vector3 spawnPosition = new Vector3(0, 0, 0);
    [SerializeField] private GameObject[] boatList;

    private Object playerBoat;

    private void Awake()
    {
        if (!PlayerPrefs.HasKey("Boat"))
        {
            PlayerPrefs.SetInt("Boat", 0);
        }
        PlayerPrefs.GetInt("Boat");

        playerBoat = Instantiate(boatList[PlayerPrefs.GetInt("Boat")], spawnPosition, Quaternion.identity);
    }

    public void selectBoat(int id)
    {
        PlayerPrefs.SetInt("Boat", id);

        Destroy(playerBoat);
        playerBoat = Instantiate(boatList[PlayerPrefs.GetInt("Boat")], spawnPosition, Quaternion.identity);
    }
}