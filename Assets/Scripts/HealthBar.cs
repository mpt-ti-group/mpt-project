﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image hpBar;
    private PlayerController pc;

    private void Awake()
    {
        hpBar = transform.Find("Foreground").GetComponent<Image>();
    }

    private void Start()
    {
        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        hpBar.fillAmount = pc.getHealthPercent();
    }
}