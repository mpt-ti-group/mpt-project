﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CammeraFollowPlayer : MonoBehaviour
{
    [SerializeField] private float cameraOffset = 5f;
    public float power = 0.3f;
    public float duration = 0.15f;
    public bool shakeEnabled = false;

    public Transform playerTransform;
    private Transform cameraTransform;
    private Vector3 returnPos;
    private float ininialDuration;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        returnPos = cameraTransform.localPosition;
        ininialDuration = duration;
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    private void LateUpdate()
    {
        Vector3 newPos = cameraTransform.localPosition;
        newPos.x = playerTransform.position.x;
        newPos.y = playerTransform.position.y + cameraOffset;
        // transform.position = newPos;
        cameraTransform.localPosition = newPos;
        returnPos = newPos;

        if (shakeEnabled)
        {
            if (duration > 0)
            {
                float scaleDownPower = duration / ininialDuration;
                cameraTransform.localPosition = returnPos + Random.insideUnitSphere * power * scaleDownPower;
                duration -= Time.deltaTime;
            }
            else
            {
                shakeEnabled = false;
                duration = ininialDuration;
                cameraTransform.localPosition = returnPos;
            }
        }
    }
}