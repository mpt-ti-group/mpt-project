﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    private PlayerController pc;
    private bool isOver = false;

    private void Start()
    {
        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (pc.getHealthPercent() <= 0 && !isOver)
        {
            Over();
            isOver = true;
        }
    }

    public void Over()
    {
        Time.timeScale = 0;
        Camera.main.GetComponent<CammeraFollowPlayer>().shakeEnabled = false;
        GetComponent<Canvas>().enabled = true;
        //pauseMenu.enabled = true;
    }

    public void RestartLevel()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ReturnMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenuScene");
    }
}