﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //SerializeField zapewnia widoczność w edytorze dla pól innych niż public

    [SerializeField] public float movementForce = 10f;
    [SerializeField] public float movementTorque = 1f;
    [SerializeField] private float turnForce = 10f;
    [SerializeField] private float turnTorque = 1f;
    [SerializeField] private const float maxHealth = 100f;

    private Rigidbody2D rb;
    private float input = 0;
    private float lastInput = 0;
    private float lastInputCandidate = 0;
    private float health = maxHealth;
    private CammeraFollowPlayer cameraScript;
    private Animator anim;

    private float lastPaddlleTime = 0f;
    private float paddleLoopTime = 0.8f;

    //przy ładowaniu skryptu, jeszcze przed Start()
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        cameraScript = Camera.main.GetComponent<CammeraFollowPlayer>();
        anim = GameObject.Find("dude").GetComponent<Animator>();
    }

    //funkcja wywoływana tak szybko jak się da
    private void Update()
    {
        ManageInput();
    }

    //funkcja wywoływana co game tick, dom. 50 razy na sekundę
    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        float magnitude = collision.relativeVelocity.magnitude;
        if (magnitude >= 1f)
        {
            SoundManager.PlaySound("hit");
            cameraScript.power = magnitude / 20f;
            cameraScript.shakeEnabled = true;
            health -= (magnitude * 2) - 1f;
        }
    }

    private void MovePlayer()
    {
        if (input != 0)
        {
            if (input == lastInput)
            {
                rb.AddTorque(-turnTorque * input, ForceMode2D.Force);
                rb.AddForce(transform.up * turnForce, ForceMode2D.Force);
            }
            else
            {
                rb.AddTorque(-movementTorque * input, ForceMode2D.Force);
                rb.AddForce(transform.up * movementForce, ForceMode2D.Force);
            }
        }
    }

    private void ManageInput()
    {
        //zaokrąglanie bo chcę -1,0,1 a może być pomiędzy
        input = Mathf.Round(Input.GetAxisRaw("Horizontal"));

        if (input != lastInputCandidate)
        {
            if (lastInputCandidate == 0)
            {
                if (input == 1)
                {
                    SoundManager.PlaySound("paddle_1");
                    anim.SetTrigger("paddleRight");
                    lastPaddlleTime = Time.time;
                }
                else if (input == -1)
                {
                    SoundManager.PlaySound("paddle_2");
                    anim.SetTrigger("paddleLeft");
                    lastPaddlleTime = Time.time;
                }
            }

            if (lastInputCandidate != 0)
            {
                lastInput = lastInputCandidate;
            }
        } else
        {
            if(Time.time - lastPaddlleTime >= paddleLoopTime)
            {
                if (input == 1)
                {
                    SoundManager.PlaySound("paddle_1");
                    anim.SetTrigger("paddleRight");
                    lastPaddlleTime = Time.time;
                }
                else if (input == -1)
                {
                    SoundManager.PlaySound("paddle_2");
                    anim.SetTrigger("paddleLeft");
                    lastPaddlleTime = Time.time;
                } else
                {
                    lastPaddlleTime = Time.time;
                }
            }
        }
        lastInputCandidate = input;
    }

    public float getHealthPercent()
    {
        return Mathf.Max(0, health / maxHealth);
    }
}