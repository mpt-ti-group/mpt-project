﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CountdownController : MonoBehaviour
{
    public Text CountdownTimer;

    private void Awake()
    {
        Time.timeScale = 0;
        StartCoroutine(CountdownCoroutine());
    }

    private IEnumerator CountdownCoroutine()
    {
        CountdownTimer.text = "3";
        yield return WaitForRealSeconds(1.0f);
        CountdownTimer.text = "2";
        yield return WaitForRealSeconds(1.0f);
        CountdownTimer.text = "1";
        yield return WaitForRealSeconds(1.0f);
        CountdownTimer.text = "Go!";
        Time.timeScale = 1;
        yield return WaitForRealSeconds(1.0f);
        CountdownTimer.enabled = false;
        //SceneManager.LoadScene("Level01");
        yield return null;
    }

    private IEnumerator WaitForRealSeconds(float seconds)
    {
        float startTime = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup - startTime < seconds)
        {
            yield return null;
        }
    }
}